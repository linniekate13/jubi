package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount


class EditProfileController(args: Bundle? = null): BaseController(args)
{
    var account: GoogleSignInAccount? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.edit_profile_controller, container, false)
        account = GoogleSignIn.getLastSignedInAccount(containerView.context)
        // use account.idToken to uniquely identify a user.
        //Log.d("TEST", account!!.idToken);
        return containerView
    }



}