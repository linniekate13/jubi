package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.create_post_controller.view.*
import kotlinx.android.synthetic.main.home_controller.view.*

class CreatePostController(args: Bundle? = null): BaseController(args)
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.create_post_controller, container, false)

        containerView.go_back_button.setOnClickListener {
            router!!.setRoot(RouterTransaction.with(HomeController()))
        }
        containerView.profile_button2.setOnClickListener {
            router!!.setRoot(RouterTransaction.with(MessagesController()))
        }
        containerView.post_button.setOnClickListener {
            // TODO: post the post to firebase and then reset the post page or go to the home page
        }

        return containerView
    }
}