package com.app.daniel.jubi

import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.FadeChangeHandler
import com.bluelinelabs.conductor.changehandler.VerticalChangeHandler

fun Router.rootFadeInVerticalOut(controller: Controller)
{
    setRoot( RouterTransaction.with( controller ).apply
    {
        pushChangeHandler( FadeChangeHandler() )
        popChangeHandler( FadeChangeHandler() )
    } )
}

fun Router.vertical(controller: Controller)
{
    pushController( RouterTransaction.with( controller ).apply {
        pushChangeHandler( VerticalChangeHandler() )
        popChangeHandler( VerticalChangeHandler() )
    } )
}