package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.view.View
import com.bluelinelabs.conductor.Controller

abstract class BaseController(args: Bundle?): Controller(args)
{
    internal lateinit var containerView: View
}