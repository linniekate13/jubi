package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit

class LogoController(args: Bundle? = null): BaseController(args)
{
    private val alreadyHasData = false
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.logo_controller, container, false)
        return containerView
    }

    override fun onAttach(view: View)
    {
        super.onAttach(view)

        compositeDisposable += Single.just( alreadyHasData )
                .delay(4, TimeUnit.SECONDS)
                .observeOn( AndroidSchedulers.mainThread() )
                .subscribe{ hasData: Boolean ->
                    when(hasData)
                    {
                        false -> router!!.setRoot( RouterTransaction.with( LoginController() ) )
                        true -> router!!.setRoot( RouterTransaction.with( HomeController() ) )
                    }
                }
    }

}