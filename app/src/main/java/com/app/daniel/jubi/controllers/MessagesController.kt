package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.app.daniel.jubi.R
import com.app.daniel.jubi.USER_EMAIL
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.messages_controller.view.*


class MessagesController(args: Bundle? = null): BaseController(args)
{
//    var account: GoogleSignInAccount? = null
    private lateinit var listAdapter: ArrayAdapter<String>
    private var listItems: ArrayList<String> = ArrayList<String>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.messages_controller, container, false)
//        account = GoogleSignIn.getLastSignedInAccount(containerView.context)
        listAdapter = ArrayAdapter<String>(containerView.context, android.R.layout.simple_list_item_1, listItems)
        containerView.listview_messages.adapter = listAdapter
        getMessages()
        return containerView
    }

    fun getMessages() {
        val db = FirebaseFirestore.getInstance()
        db.collection("users").whereEqualTo("email", USER_EMAIL)
                .get()
                .addOnCompleteListener{
                    if (it.isSuccessful) {
                        if (!it.result.isEmpty) {
                            val docId = it.result.documents[0].id
                            db.collection("users").document(docId).collection("messages")
                                    .get()
                                    .addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            for (document in it.result) {
                                                listItems.add(document.data["content"].toString())
                                                listAdapter.notifyDataSetChanged()
                                                Log.w("TEST", document.data["content"].toString())
                                            }
                                        }
                                    }
                        }
                    }
                }
    }

}