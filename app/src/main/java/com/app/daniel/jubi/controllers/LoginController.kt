package com.app.daniel.jubi.controllers

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.login_controller.view.*



class LoginController(args: Bundle? = null): BaseController(args)
{
    private val RC_SIGN_IN = 9001
    lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {

        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(resources!!.getString(R.string.web_client_id))
                .requestEmail()
                .build()

        containerView = inflater.inflate(R.layout.login_controller, container, false)

        mGoogleSignInClient = GoogleSignIn.getClient(containerView.context, gso)
        var account = GoogleSignIn.getLastSignedInAccount(containerView.context)

        containerView.sign_in_button.setOnClickListener{
            signIn()
        }

        return containerView
    }

    private fun signIn() {
        // Uncomment to perform real login
//        val signInIntent = mGoogleSignInClient.signInIntent;
//        startActivityForResult(signInIntent, RC_SIGN_IN);
        router!!.setRoot(RouterTransaction.with(HomeController()))
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            Log.w("TEST", "signed in succesfully id: " + account.idToken.toString())
            var db = FirebaseFirestore.getInstance()
            // Check if new users
            db.collection("users").whereEqualTo("email", account.email)
                    .get()
                    .addOnCompleteListener{
                        if (it.isSuccessful) {
                            Log.w("TEST", "was success. Empty? " + it.result.isEmpty)
                            for (document in it.result) {
                                Log.w("TEST", document.data["hasOnboarded"].toString())
                                if (document.data["hasOnboarded"].toString() == "true") {
                                    // user exists and has onboarded
                                    router!!.setRoot( RouterTransaction.with( MessagesController() ) )
                                } else {
                                    router!!.setRoot( RouterTransaction.with( EditProfileController() ) )
                                }
                            }
                            if (it.result.isEmpty) {
                                router!!.setRoot( RouterTransaction.with( EditProfileController() ) )
                            }
                        } else {
                            router!!.setRoot( RouterTransaction.with( EditProfileController() ) )
                        }
                    }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TEST", "signInResult:failed code=" + e.statusCode)
            //updateUI(null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            var task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }
}