package com.app.daniel.jubi.controllers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.home_controller.view.*

class ProfileController: Controller()
{
    lateinit var containerView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.home_controller, container, false)
        return containerView
    }
}