package com.app.daniel.jubi.controllers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.daniel.jubi.R
import com.bluelinelabs.conductor.RouterTransaction
import kotlinx.android.synthetic.main.home_controller.view.*

class HomeController(args: Bundle? = null): BaseController(args)
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View
    {
        containerView = inflater.inflate(R.layout.home_controller, container, false)

        containerView.create_post_button.setOnClickListener {
            router!!.setRoot(RouterTransaction.with(CreatePostController()))
        }
        containerView.home_profile_button.setOnClickListener {
            router!!.setRoot(RouterTransaction.with(MessagesController()))
        }

        return containerView
    }

}